provider "local" { }

resource "null_resource" "print_numbers" {
  count = 10

  provisioner "local-exec" {
    command = "echo ${count.index + 1}"
  }
}

output "printed_numbers" {
  value = [for n in null_resource.print_numbers: n.id]
}
